import React from 'react';
import Spinner from "./Spinner";
import {WalletValue} from "./WalletValue";
import {WalletChart} from "./WalletChart";
import Assets from "./Assets";

class Application extends React.Component
{
    state = {
        dataIsLoading: false,
        walletValue: undefined,
        walletChartData: undefined,
        assets: []
    };

    componentDidMount() {
        this.setState({dataIsLoading: true});

        fetch('/api/summary').then(result => {
            return result.json();
        }).then(data => {
            this.setState({
                walletValue: data.wallet.value,
                dataIsLoading: false,
                walletChartData: data.history,
                assets: data.assets
            })
        })
    }

    handleAssetClick = (e) => {
        console.log(e.target);
    };

    render() {
        return (
            <>
                <Spinner show={this.state.dataIsLoading}/>
                <WalletValue value={this.state.walletValue} />
                <div className="container">
                    <Assets assets={this.state.assets} handleAssetClick={this.handleAssetClick} />
                    <WalletChart data={this.state.walletChartData}/>
                </div>
            </>
        )
    }
}

export default Application;