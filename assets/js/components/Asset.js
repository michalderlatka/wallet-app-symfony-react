import React from 'react';

class Asset extends React.Component
{
    render() {
        const { name, value, change24h, change7d } = this.props.asset;

        return (
            <tr>
                <td className="text-center">{name}</td>
                <td className="text-center">{value}</td>
                <td className={"text-center d-none d-sm-block " + (change24h >= 0 ? 'text-success' : 'text-danger')}>{change24h > 0 ? "+" : "-"}{change24h.toFixed(2)}%</td>
                <td className={"text-center " + (change7d >= 0 ? 'text-success' : 'text-danger')}>{change7d > 0 ? "+" : "-"}{change7d.toFixed(2)}%</td>
                <td style={{cursor: "pointer"}}><i className="fas fa-chart-area"></i></td>
            </tr>
        )
    }
}

export default Asset;