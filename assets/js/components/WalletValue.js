import React from 'react';

export const WalletValue = (props) => (
    <div id="wallet-value">
        <div className="container">
            <div className="row">
                <div className="col-sm text-center">
                    <h1 className="display-1">{typeof props.value !== "undefined" && parseInt(props.value)}</h1>
                </div>
            </div>
        </div>
    </div>
);