import React from 'react';
import Asset from './Asset';

class Assets extends React.Component
{
    render = () => (
            <div className="table-responsive">
                <table className="table table-striped">
                    <tbody>
                        {this.props.assets.sort((a,b) => b.value - a.value).map((val, index) => {
                            return <Asset asset={val} key={val.id} handleAssetClick={this.props.handleAssetClick} />
                        })}
                    </tbody>
                </table>
            </div>
    )
}

export default Assets;