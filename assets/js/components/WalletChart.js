import { AreaChart, Area, XAxis, YAxis, ResponsiveContainer } from "recharts";
import React from 'react';

export const WalletChart = (props) => (
    <ResponsiveContainer width="100%" height={400}>
        <AreaChart data={typeof props.data !== "undefined" ? props.data.reverse().map((v, i) => ({
                value: v.value,
                date: new Date(v.createdAt).getDate()
            })) : []}>
            <Area type="monotone" dataKey="value" stroke="#8D8741" fill="#8D8741" />
            <XAxis dataKey="date" />
            <YAxis domain={['auto', 'auto']}/>
        </AreaChart>
    </ResponsiveContainer>
);
