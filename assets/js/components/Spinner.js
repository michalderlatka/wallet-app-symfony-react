import React from 'react';

const Spinner = (props) => (
    <div className="container">
        <div className="text-center" hidden={!props.show}>
            <div className="spinner-grow text-success" role="status">
                <span className="sr-only">Loading data...</span>
            </div>
        </div>
    </div>
);

export default Spinner;
