<?php

namespace App\Repository;

use App\Entity\WalletAssetHistory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method WalletAssetHistory|null find($id, $lockMode = null, $lockVersion = null)
 * @method WalletAssetHistory|null findOneBy(array $criteria, array $orderBy = null)
 * @method WalletAssetHistory[]    findAll()
 * @method WalletAssetHistory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WalletAssetHistoryRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, WalletAssetHistory::class);
    }

    // /**
    //  * @return WalletAssetHistory[] Returns an array of WalletAssetHistory objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?WalletAssetHistory
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
