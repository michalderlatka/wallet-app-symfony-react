<?php

namespace App\Repository;

use App\Entity\WalletHistory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method WalletHistory|null find($id, $lockMode = null, $lockVersion = null)
 * @method WalletHistory|null findOneBy(array $criteria, array $orderBy = null)
 * @method WalletHistory[]    findAll()
 * @method WalletHistory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WalletHistoryRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, WalletHistory::class);
    }

    /**
     * @return WalletHistory
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findByLastEntry()
    {
        return $this->createQueryBuilder('w')
            ->orderBy('w.createdAt', 'desc')
            ->setMaxResults(1)
            ->getQuery()
            ->getSingleResult();
    }

    /**
     * @return WalletHistory[]
     * @throws \Exception
     */
    public function findEntriesFromLastMonth()
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.createdAt > :date')
            ->orderBy('w.createdAt', 'desc')
            ->setParameter('date', new \DateTime('-1 month'))
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return WalletHistory[] Returns an array of WalletHistory objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?WalletHistory
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
