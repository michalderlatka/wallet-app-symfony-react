<?php
/**
 * Created by PhpStorm.
 * User: Michal
 * Date: 10.03.2019
 * Time: 14:57
 */

namespace App\Command;

use App\Entity\Asset;
use App\Entity\WalletAssetHistory;
use App\Entity\WalletHistory;
use App\Model\UpdaterManager;
use App\Repository\AssetRepository;
use App\Repository\WalletAssetHistoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateAssetsPriceCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:update-assets';
    /**
     * @var UpdaterManager
     */
    private $updater;
    /**
     * @var AssetRepository
     */
    private $assetRepository;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(
        AssetRepository $assetRepository,
        UpdaterManager $updaterManager,
        EntityManagerInterface $entityManager,
        string $name = null
    )
    {
        parent::__construct($name);

        $this->updater = $updaterManager;
        $this->assetRepository = $assetRepository;
        $this->entityManager = $entityManager;
    }

    protected function configure()
    {
        $this
            ->setDescription('Updates assets prices.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Asset[] $assets */
        $assets = $this->assetRepository->findAll();

        $totalWalletValue = 0;

        foreach ($assets as $asset) {
            $asset = $this->updater->updateAsset($asset, function ($asset) use ($output) {
                $output->writeln($asset->getCode() . " - " . $asset->getPrice());
            });

            $this->entityManager->persist($asset);

            $assetHistory = new WalletAssetHistory();
            $assetHistory->setCreatedAt(new \DateTime());
            $assetHistory->setValue($asset->getValue());
            $assetHistory->setPrice($asset->getPrice());
            $assetHistory->setAsset($asset);

            $this->entityManager->persist($assetHistory);

            $totalWalletValue += $asset->getValue();
        }

        $walletHistory = new WalletHistory();
        $walletHistory->setValue($totalWalletValue);
        $walletHistory->setCreatedAt(new \DateTime());

        $this->entityManager->persist($walletHistory);

        $this->entityManager->flush();
    }

}