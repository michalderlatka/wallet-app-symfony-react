<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190310133815 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE asset (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, code VARCHAR(255) NOT NULL, quantity DOUBLE PRECISION NOT NULL, price NUMERIC(10, 3) NOT NULL, value NUMERIC(10, 2) NOT NULL, change24h DOUBLE PRECISION NOT NULL, change7d DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE wallet_asset_history (id INT AUTO_INCREMENT NOT NULL, asset_id INT NOT NULL, created_at DATETIME NOT NULL, value NUMERIC(10, 2) NOT NULL, price NUMERIC(10, 3) NOT NULL, INDEX IDX_757515ED5DA1941 (asset_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE wallet_history (id INT AUTO_INCREMENT NOT NULL, created_at DATETIME NOT NULL, value NUMERIC(10, 2) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE wallet_asset_history ADD CONSTRAINT FK_757515ED5DA1941 FOREIGN KEY (asset_id) REFERENCES asset (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE wallet_asset_history DROP FOREIGN KEY FK_757515ED5DA1941');
        $this->addSql('DROP TABLE asset');
        $this->addSql('DROP TABLE wallet_asset_history');
        $this->addSql('DROP TABLE wallet_history');
    }
}
