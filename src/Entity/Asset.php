<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AssetRepository")
 */
class Asset
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $code;

    /**
     * @ORM\Column(type="float")
     */
    private $quantity;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=3)
     */
    private $price;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $value;

    /**
     * @ORM\Column(type="float")
     */
    private $change24h;

    /**
     * @ORM\Column(type="float")
     */
    private $change7d;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\WalletAssetHistory", mappedBy="asset")
     */
    private $walletAssetHistories;

    public function __construct()
    {
        $this->walletAssetHistories = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getQuantity(): ?float
    {
        return $this->quantity;
    }

    public function setQuantity(float $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function setValue($value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getChange24h(): ?float
    {
        return $this->change24h;
    }

    public function setChange24h(float $change24h): self
    {
        $this->change24h = $change24h;

        return $this;
    }

    public function getChange7d(): ?float
    {
        return $this->change7d;
    }

    public function setChange7d(float $change7d): self
    {
        $this->change7d = $change7d;

        return $this;
    }

    /**
     * @return Collection|WalletAssetHistory[]
     */
    public function getWalletAssetHistories(): Collection
    {
        return $this->walletAssetHistories;
    }

    public function addWalletAssetHistory(WalletAssetHistory $walletAssetHistory): self
    {
        if (!$this->walletAssetHistories->contains($walletAssetHistory)) {
            $this->walletAssetHistories[] = $walletAssetHistory;
            $walletAssetHistory->setAsset($this);
        }

        return $this;
    }

    public function removeWalletAssetHistory(WalletAssetHistory $walletAssetHistory): self
    {
        if ($this->walletAssetHistories->contains($walletAssetHistory)) {
            $this->walletAssetHistories->removeElement($walletAssetHistory);
            // set the owning side to null (unless already changed)
            if ($walletAssetHistory->getAsset() === $this) {
                $walletAssetHistory->setAsset(null);
            }
        }

        return $this;
    }
}
