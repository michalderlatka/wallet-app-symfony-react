<?php
/**
 * Created by PhpStorm.
 * User: Michal
 * Date: 10.03.2019
 * Time: 19:17
 */

namespace App\Model;


use App\Entity\Asset;

interface UpdaterInterface
{
    public function update(Asset $asset) : Asset;
}