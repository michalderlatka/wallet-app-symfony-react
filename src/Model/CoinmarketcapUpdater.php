<?php
/**
 * Created by PhpStorm.
 * User: Michal
 * Date: 10.03.2019
 * Time: 19:19
 */

namespace App\Model;

use App\Entity\Asset;
use Http\Message\MessageFactory;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class CoinmarketcapUpdater implements UpdaterInterface
{
    private $endpointUrl;
    private $apiKey;
    /** @var MessageFactory */
    private $messageFactory;
    private $httpClient;

    public function __construct($endpointUrl, $apiKey, $messageFactory, $httpClient)
    {
        $this->endpointUrl = $endpointUrl;
        $this->apiKey = $apiKey;
        $this->messageFactory = $messageFactory;
        $this->httpClient = $httpClient;
    }

    public function update(Asset $asset): Asset
    {
        /** @var RequestInterface $request */
        $request = $this->messageFactory->createRequest(
            'GET',
            $this->buildUrl($asset)
        );
        $request = $request->withHeader('X-CMC_PRO_API_KEY', $this->apiKey);

        /** @var ResponseInterface $response */
        $response = $this->httpClient->sendRequest($request);
        $body = json_decode($response->getBody()->getContents(), true);

        if (isset($body['data'][$asset->getCode()]['quote']['PLN'])) {
            $quote = $body['data'][$asset->getCode()]['quote']['PLN'];

            $asset->setPrice($quote['price']);
            $asset->setValue($quote['price'] * $asset->getQuantity());
            $asset->setChange24h($quote['percent_change_24h']);
            $asset->setChange7d($quote['percent_change_7d']);
        }

        return $asset;
    }

    protected function buildUrl(Asset $asset)
    {
        $url = $this->endpointUrl;
        $url .= "/v1/cryptocurrency/quotes/latest?";

        $url .= http_build_query([
            'symbol' => $asset->getCode(),
            'convert' => 'PLN'
        ]);

        return $url;
    }

}