<?php
/**
 * Created by PhpStorm.
 * User: Michal
 * Date: 10.03.2019
 * Time: 19:47
 */

namespace App\Model;


use App\Entity\Asset;
use Doctrine\ORM\EntityManagerInterface;

class UpdaterManager
{
    /**
     * @var UpdaterInterface
     */
    private $updater;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(UpdaterInterface $updater, EntityManagerInterface $entityManager)
    {
        $this->updater = $updater;
        $this->entityManager = $entityManager;
    }

    /**
     * @param Asset $asset
     * @param callable|null $callback
     * @return Asset
     */
    public function updateAsset(Asset $asset, callable $callback = null)
    {
        $this->updater->update($asset);

        if ($callback) {
            $callback($asset);
        }

        return $asset;
    }
}