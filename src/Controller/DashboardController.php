<?php
/**
 * Created by PhpStorm.
 * User: Michal
 * Date: 24.03.2019
 * Time: 17:13
 */

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractController
{
    /**
     * @Route("/", name="front_dashboard")
     */
    public function index()
    {
        return $this->render('dashboard.html.twig');
    }
}