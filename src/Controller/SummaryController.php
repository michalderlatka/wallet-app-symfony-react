<?php
/**
 * Created by PhpStorm.
 * User: Michal
 * Date: 13.03.2019
 * Time: 21:43
 */

namespace App\Controller;


use App\Repository\AssetRepository;
use App\Repository\WalletHistoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class SummaryController extends AbstractController
{

    /**
     * @var AssetRepository
     */
    private $assetRepository;
    /**
     * @var WalletHistoryRepository
     */
    private $walletHistoryRepository;

    public function __construct(
        AssetRepository $assetRepository,
        WalletHistoryRepository $walletHistoryRepository
    )
    {
        $this->assetRepository = $assetRepository;
        $this->walletHistoryRepository = $walletHistoryRepository;
    }

    /**
     * @Route("/api/summary", name="api_wallet_summary")
     */
    public function walletSummary()
    {
        $lastWallet = $this->walletHistoryRepository->findByLastEntry();

        if ($lastWallet) {
            $wallet = [
                'value' => $lastWallet->getValue(),
                'createdAt' => $lastWallet->getCreatedAt()->format(DATE_ISO8601)
            ];
        }

        $lastMonth = $this->walletHistoryRepository->findEntriesFromLastMonth();

        $history = [];

        foreach ($lastMonth as $item) {
            $history[] = [
                'value' => $item->getValue(),
                'createdAt' => $item->getCreatedAt()->format(DATE_ISO8601)
            ];
        }

        $assets = $this->assetRepository->findAll();
        $assetReturn = [];

        foreach ($assets as $asset) {
            $assetReturn[] = [
                'id' => $asset->getId(),
                'name' => $asset->getName(),
                'qty' => $asset->getQuantity(),
                'price' => $asset->getPrice(),
                'value' => $asset->getValue(),
                'change24h' => $asset->getChange24h(),
                'change7d' => $asset->getChange7d()
            ];
        }

        return $this->json([
            'wallet' => $wallet ?? [],
            'history' => $history,
            'assets' => $assetReturn
        ]);
    }
}